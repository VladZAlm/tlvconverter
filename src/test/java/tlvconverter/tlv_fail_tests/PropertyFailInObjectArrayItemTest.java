package tlvconverter.tlv_fail_tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.ItemCreator;
import tlvconverter.property_data_containers.PropertyType;
import tlvconverter.tlv_success_tests.ObjectPropertyTest;

public class PropertyFailInObjectArrayItemTest {
	@Test
	public void test() {
		byte[] bytes = { 1, 0, 7, 0, 2, 0, 3, 0, 4, 113, 2, 1, 0, 9, 0, 2, 0, 3, 0, 4, 113, 3, 3, 0 };

		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();
		ItemCreator itemCreator = ObjectPropertyTest.getItemCreator();
		PropertyType propertyType = new PropertyType("arr", 1, itemCreator);
		propertyTypes.put(propertyType.getTag(), propertyType);
		
		TlvFailTestsUtils.assertData(bytes, 22, propertyTypes);
	}
}
