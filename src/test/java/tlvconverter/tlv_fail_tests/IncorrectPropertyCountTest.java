package tlvconverter.tlv_fail_tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.property_data_containers.PropertyType;

public class IncorrectPropertyCountTest {
	@Test
	public void test() {
		byte[] bytes = { 1, 0, 4, 0, 64, (byte) 156, 115, 30 };
		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();

		PropertyType propertyType = new PropertyType("vln1", 1, new VlnCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		propertyType = new PropertyType("vln2", 2, new VlnCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		TlvFailTestsUtils.assertData(bytes, 0, propertyTypes);
	}
}
