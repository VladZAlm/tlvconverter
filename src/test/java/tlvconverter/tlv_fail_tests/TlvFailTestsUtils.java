package tlvconverter.tlv_fail_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Map;

import tlvconverter.TestUtils;
import tlvconverter.TlvItemConverter;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class TlvFailTestsUtils {
	public static void assertData(byte[] bytes, int expectedTagPosition, Map<Integer, PropertyType> propertyTypes) {
		TlvItemConverter itemConverter = TestUtils.createTlvItemConverter(propertyTypes);

		IncorrectItemDataException e = assertThrows(IncorrectItemDataException.class,
				() -> itemConverter.convert(bytes));

		int expected = expectedTagPosition;
		int actual = e.getTagPosition();
		assertEquals(expected, actual);
	}
}
