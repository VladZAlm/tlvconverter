package tlvconverter.tlv_success_tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.DateCreator;
import tlvconverter.data_type_creators.FvlnCreator;
import tlvconverter.data_type_creators.StringCreator;
import tlvconverter.data_type_creators.Uint32Creator;
import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class SimpleTypesPropertiesTest {
	@Test
	public void test() throws IncorrectItemDataException {
		byte[] bytes = { 1, 0, 4, 0, (byte) 168, 50, (byte) 146, 86, 2, 0, 3, 0, 4, 113, 2, 3, 0, 11, 0, (byte) 142,
				(byte) 142, (byte) 142, 32, (byte) 144, (byte) 174, (byte) 172, (byte) 160, (byte) 232, (byte) 170,
				(byte) 160, 4, 0, 2, 0, 0, 2, 5, 0, 4, 0, (byte) 168, 50, (byte) 146, 86 };

		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();

		PropertyType propertyType = new PropertyType("date", 1, new DateCreator("yyyy-MM-dd'T'HH:mm:ss"));
		propertyTypes.put(propertyType.getTag(), propertyType);

		propertyType = new PropertyType("vln", 2, new VlnCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		propertyType = new PropertyType("string", 3, new StringCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		propertyType = new PropertyType("fvln", 4, new FvlnCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		propertyType = new PropertyType("uint32", 5, new Uint32Creator());
		propertyTypes.put(propertyType.getTag(), propertyType);

		String expected = "{\"date\":\"2016-01-10T10:30:00\",\"vln\":160004,\"fvln\":2,\"uint32\":1452421800,\"string\":\"ООО Ромашка\"}";
		TlvSuccessTestsUtils.assertTlvData(bytes, expected, propertyTypes);
	}
}
