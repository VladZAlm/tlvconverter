package tlvconverter.tlv_success_tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.DataTypeCreator;
import tlvconverter.data_type_creators.ItemCreator;
import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class ArrayPropertyTest {
	@Test
	public void arrayWithOneItemTest() throws IncorrectItemDataException {
		Map<Integer, PropertyType> propertyTypes = getPropertyTypes(new VlnCreator());
		String expected = "{\"arr\":[" + getItemString() + "]}";
		TlvSuccessTestsUtils.assertTlvData(getItemBytes(), expected, propertyTypes);
	}

	@Test
	public void arrayWithObjectItemTest() throws IncorrectItemDataException {
		byte[] bytes = ObjectPropertyTest.getBytes();

		ItemCreator itemCreator = ObjectPropertyTest.getItemCreator();
		Map<Integer, PropertyType> propertyTypes = getPropertyTypes(itemCreator);

		String stringObject = ObjectPropertyTest.getStringObjectProperty();
		String expected = "{\"arr\":[" + stringObject + "]}";
		TlvSuccessTestsUtils.assertTlvData(bytes, expected, propertyTypes);
	}

	@Test
	public void arrayWithSeveralItemsTest() throws IncorrectItemDataException {
		byte[] bytes = new byte[getItemBytes().length * 2];
		byte[] itemBytes = getItemBytes();
		for (int i = 0; i < itemBytes.length; i++) {
			byte b = itemBytes[i];
			bytes[i] = b;
			bytes[i + itemBytes.length] = b;
		}

		Map<Integer, PropertyType> propertyTypes = getPropertyTypes(new VlnCreator());

		String expected = String.format("{\"arr\":[%s,%s]}", getItemString(), getItemString());
		TlvSuccessTestsUtils.assertTlvData(bytes, expected, propertyTypes);
	}

	private byte[] getItemBytes() {
		byte[] bytes = { 1, 0, 2, 0, 64, (byte) 156 };
		return bytes;
	}

	private String getItemString() {
		return "40000";
	}

	private Map<Integer, PropertyType> getPropertyTypes(DataTypeCreator<?> dataCreator) {
		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();
		dataCreator.setIsArray(true);
		PropertyType propertyType = new PropertyType("arr", 1, dataCreator);
		propertyTypes.put(propertyType.getTag(), propertyType);
		return propertyTypes;
	}
}
