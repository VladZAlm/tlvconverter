package tlvconverter.tlv_success_tests;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import tlvconverter.TestUtils;
import tlvconverter.TlvItemConverter;
import tlvconverter.data_type_creators.ItemCreator;
import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class ObjectPropertyTest {
	@Test
	public void test() throws IncorrectItemDataException {
		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();
		ItemCreator itemCreator = getItemCreator();
		PropertyType propertyType = new PropertyType("obj", 1, itemCreator);
		propertyTypes.put(propertyType.getTag(), propertyType);

		String expected = "{\"obj\":" + getStringObjectProperty() + "}";
		TlvSuccessTestsUtils.assertTlvData(getBytes(), expected, propertyTypes);
	}

	public static ItemCreator getItemCreator() {
		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();
		PropertyType propertyType = new PropertyType("vln", 2, new VlnCreator());
		propertyTypes.put(propertyType.getTag(), propertyType);
		TlvItemConverter itemConverter = TestUtils.createTlvItemConverter(propertyTypes);
		return new ItemCreator(itemConverter);
	}

	public static byte[] getBytes() {
		byte[] bytes = { 1, 0, 7, 0, 2, 0, 3, 0, 4, 113, 2 };
		return bytes;
	}

	public static String getStringObjectProperty() {
		return "{\"vln\":160004}";
	}
}
