package tlvconverter.tlv_success_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import tlvconverter.TestUtils;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class TlvSuccessTestsUtils {
	public static void assertTlvData(byte[] bytes, String expected, Map<Integer, PropertyType> propertyTypes)
			throws IncorrectItemDataException {
		String actual = TestUtils.createTlvItemConverter(propertyTypes).convert(bytes).toString();
		assertEquals(expected, actual);
	}
}
