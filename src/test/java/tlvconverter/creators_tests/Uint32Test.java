package tlvconverter.creators_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.Uint32Creator;
import tlvconverter.exceptions.IncorrectDataException;

public class Uint32Test {
	@Test
	public void successTest() throws IncorrectDataException {
		byte[] bytes = { -88, 50, -110, 86 };
		Uint32Creator uint32Creator = new Uint32Creator();

		long expected = 1452421800;
		long actual = uint32Creator.create(bytes).longValue();

		assertEquals(expected, actual);
	}

	@Test
	public void incorrectBytesLengthTest() {
		byte[] bytes = { -88, 50, -110 };
		Uint32Creator uint32Creator = new Uint32Creator();
		assertThrows(IncorrectDataException.class, () -> uint32Creator.create(bytes));
	}
}
