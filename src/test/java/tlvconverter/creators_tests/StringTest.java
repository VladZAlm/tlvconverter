package tlvconverter.creators_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.StringCreator;
import tlvconverter.exceptions.IncorrectDataException;

public class StringTest {
	@Test
	public void successTest() throws IncorrectDataException {
		byte[] bytes = { -114, -114, -114, 32, -112, -82, -84, -96, -24, -86, -96 };
		StringCreator stringCreator = new StringCreator();

		String expected = "ООО Ромашка";
		String actual = stringCreator.create(bytes);

		assertEquals(expected, actual);
	}
}
