package tlvconverter.creators_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.DateCreator;
import tlvconverter.exceptions.IncorrectDataException;

public class DateTest {
	@Test
	public void successTest() throws IncorrectDataException {
		Date date = new Date(1452421800000L);
		String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		DateCreator dateCreator = new DateCreator(dateFormat);

		String expected = formatter.format(date);
		String actual = dateCreator.create(getBytes());

		assertEquals(expected, actual);
	}

	@Test
	public void failTest() {
		DateCreator incorrectFormat = new DateCreator("yyyy-MM-ddTHH:mm:ss");
		DateCreator nullFormat = new DateCreator(null);

		assertThrows(IncorrectDataException.class, () -> incorrectFormat.create(getBytes()));
		assertThrows(IncorrectDataException.class, () -> nullFormat.create(getBytes()));
	}

	private byte[] getBytes() {
		byte[] bytes = { -88, 50, -110, 86 };
		return bytes;
	}
}
