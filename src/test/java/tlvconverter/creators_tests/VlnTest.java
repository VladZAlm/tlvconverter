package tlvconverter.creators_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.exceptions.IncorrectDataException;

public class VlnTest {
	@Test
	public void successTest() throws IncorrectDataException {
		byte[] bytes = { 4, 113, 2 };
		VlnCreator vlnCreator = new VlnCreator();

		int expected = 160004;
		int actual = vlnCreator.create(bytes).intValue();

		assertEquals(expected, actual);
	}
}
