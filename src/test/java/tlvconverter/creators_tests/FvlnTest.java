package tlvconverter.creators_tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import tlvconverter.data_type_creators.FvlnCreator;
import tlvconverter.exceptions.IncorrectDataException;

public class FvlnTest {
	@Test
	public void successTest() throws IncorrectDataException {
		byte[] bytes = { 1, 5 };
		FvlnCreator fvlnCreator = new FvlnCreator();

		float expected = 0.5F;
		float actual = fvlnCreator.create(bytes).floatValue();

		assertEquals(expected, actual);
	}

	@Test
	public void incorrectPointTest() {
		byte[] bytes = { -1, 5 };
		FvlnCreator fvlnCreator = new FvlnCreator();
		assertThrows(IncorrectDataException.class, () -> fvlnCreator.create(bytes));
	}
}
