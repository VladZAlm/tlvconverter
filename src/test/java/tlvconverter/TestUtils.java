package tlvconverter;

import java.util.Map;

import tlvconverter.TlvItemConverter;
import tlvconverter.property_data_containers.PropertyType;

public class TestUtils {
	private static final int TAG_BYTES_LENGTH = 2;
	private static final int LENGTH_BYTES_LENGTH = 2;

	public static TlvItemConverter createTlvItemConverter(Map<Integer, PropertyType> propertyTypes) {
		return new TlvItemConverter(TAG_BYTES_LENGTH, LENGTH_BYTES_LENGTH, propertyTypes);
	}
}
