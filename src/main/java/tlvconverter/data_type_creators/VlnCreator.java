package tlvconverter.data_type_creators;

import java.math.BigInteger;

import tlvconverter.exceptions.IncorrectDataException;

public class VlnCreator extends DataTypeCreator<BigInteger> {
	private static final String TYPE_NAME = "VLN";

	@Override
	protected BigInteger createData(byte[] bytes) throws IncorrectDataException {
		return BigIntegerCreator.createUnsigned(bytes);
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
