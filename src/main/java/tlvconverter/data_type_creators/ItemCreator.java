package tlvconverter.data_type_creators;

import org.json.JSONObject;

import tlvconverter.TlvItemConverter;
import tlvconverter.exceptions.IncorrectDataException;

public class ItemCreator extends DataTypeCreator<JSONObject> {
	private static final String TYPE_NAME = "Item";

	private TlvItemConverter itemConverter;

	public ItemCreator(TlvItemConverter itemConverter) {
		this.itemConverter = itemConverter;
	}

	@Override
	protected JSONObject createData(byte[] bytes) throws IncorrectDataException {
		return itemConverter.convert(bytes, true);
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
