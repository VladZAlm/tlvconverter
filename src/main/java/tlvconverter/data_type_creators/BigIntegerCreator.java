package tlvconverter.data_type_creators;

import java.math.BigInteger;

public class BigIntegerCreator {
	public static BigInteger createUnsigned(byte[] bytes) {
		byte[] preparedBytes = new byte[bytes.length + 1];
		preparedBytes[0] = 0;
		int i1 = 1;
		for (int i2 = bytes.length - 1; i2 >= 0; i2--) {
			preparedBytes[i1] = bytes[i2];
			i1++;
		}
		return new BigInteger(preparedBytes);
	}
}
