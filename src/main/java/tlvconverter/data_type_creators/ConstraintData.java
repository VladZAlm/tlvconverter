package tlvconverter.data_type_creators;

public class ConstraintData {
	private ConstraintType constraintType;
	private int constraint;

	public ConstraintData(ConstraintType constraintType, int constraint) {
		super();
		this.constraintType = constraintType;
		this.constraint = constraint;
	}

	public ConstraintType getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(ConstraintType constraintType) {
		this.constraintType = constraintType;
	}

	public int getConstraint() {
		return constraint;
	}

	public void setConstraint(int constraint) {
		this.constraint = constraint;
	}

}
