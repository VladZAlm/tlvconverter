package tlvconverter.data_type_creators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import tlvconverter.exceptions.IncorrectDataException;

public class DateCreator extends DataTypeCreator<String> {
	private static final String TYPE_NAME = "Date";

	private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("UTC");

	private String dateFormat;

	public DateCreator(String dateFormat) {
		super();
		this.dateFormat = dateFormat;
	}

	@Override
	protected String createData(byte[] bytes) throws IncorrectDataException {
		Uint32Creator uint32Creator = new Uint32Creator();
		long data = uint32Creator.create(bytes);
		if (dateFormat == null) {
			throw new IncorrectDataException("Date format is null");
		}
		try {
			Date date = new Date(data * 1000);
			SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
			formatter.setTimeZone(TIME_ZONE);
			return formatter.format(date);
		} catch (IllegalArgumentException e) {
			String message = String.format("Incorrect date format: %s. %s", dateFormat, e.getMessage());
			throw new IncorrectDataException(message);
		}
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
