package tlvconverter.data_type_creators;

import java.math.BigInteger;

public class Uint32Creator extends DataTypeCreator<Long> {
	private static final String TYPE_NAME = "UINT32";

	private static final ConstraintType CONSTRAINT_TYPE = ConstraintType.ACCURATE;
	private static final int CONSTRAINT = 4;
	private static final ConstraintData CONSTRAINT_DATA = new ConstraintData(CONSTRAINT_TYPE, CONSTRAINT);

	@Override
	protected Long createData(byte[] bytes) {
		BigInteger bigInteger = BigIntegerCreator.createUnsigned(bytes);
		return bigInteger.longValue();
	}

	@Override
	protected ConstraintData getConstraintData() {
		return CONSTRAINT_DATA;
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
