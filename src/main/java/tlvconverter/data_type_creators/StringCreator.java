package tlvconverter.data_type_creators;

import java.io.UnsupportedEncodingException;

import tlvconverter.exceptions.IncorrectDataException;

public class StringCreator extends DataTypeCreator<String> {
	private static final String TYPE_NAME = "String";

	private static final String CP_866_ENCODING = "Cp866";

	@Override
	protected String createData(byte[] bytes) throws IncorrectDataException {
		try {
			return new String(bytes, CP_866_ENCODING);
		} catch (UnsupportedEncodingException e) {
			throw new IncorrectDataException(e);
		}
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
