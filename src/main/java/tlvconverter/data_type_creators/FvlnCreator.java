package tlvconverter.data_type_creators;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

import tlvconverter.exceptions.IncorrectDataException;

public class FvlnCreator extends DataTypeCreator<BigDecimal> {
	private static final String TYPE_NAME = "FVLN";

	@Override
	protected BigDecimal createData(byte[] bytes) throws IncorrectDataException {
		int pointPosition = bytes[0];
		if (pointPosition < 0) {
			throw new IncorrectDataException("Incorrect FVLN point position: " + pointPosition);
		}
		byte[] bigIntegerBytes = Arrays.copyOfRange(bytes, 1, bytes.length);
		BigInteger bigInteger = BigIntegerCreator.createUnsigned(bigIntegerBytes);
		return new BigDecimal(bigInteger, pointPosition);
	}

	@Override
	protected String getTypeName() {
		return TYPE_NAME;
	}
}
