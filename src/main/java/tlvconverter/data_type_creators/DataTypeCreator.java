package tlvconverter.data_type_creators;

import tlvconverter.exceptions.IncorrectDataException;
import tlvconverter.property_data_containers.PropertyInfo;

public abstract class DataTypeCreator<T> {

	private boolean isArray = false;

	public boolean getIsArray() {
		return isArray;
	}

	public void setIsArray(boolean isArray) {
		this.isArray = isArray;
	}

	public T create(byte[] bytes) throws IncorrectDataException {
		return create(bytes, null);
	}

	public T create(byte[] bytes, PropertyInfo propertyInfo) throws IncorrectDataException {
		if (propertyInfo != null && propertyInfo.getMaxLength() != null) {
			int maxLength = propertyInfo.getMaxLength();
			String name = propertyInfo.getName();
			ConstraintData propertyConstraintData = new ConstraintData(ConstraintType.MAX, maxLength);
			checkConstraintData(name, bytes.length, propertyConstraintData);
		}
		checkConstraintData(getTypeName(), bytes.length, getConstraintData());
		return createData(bytes);
	}

	protected ConstraintData getConstraintData() {
		return null;
	}

	private void checkConstraintData(String dataTypeName, int bytesLength, ConstraintData constraintData)
			throws IncorrectDataException {
		if (constraintData == null) {
			return;
		}
		checkByteLength(dataTypeName, bytesLength, constraintData.getConstraintType(), constraintData.getConstraint());
	}

	private static void checkByteLength(String dataTypeName, int bytesLength, ConstraintType constraintType,
			int constraint) throws IncorrectDataException {
		switch (constraintType) {
		case ACCURATE:
			if (bytesLength == constraint) {
				break;
			}
			throw createIncorrectByteLengthException(dataTypeName, constraintType, constraint, bytesLength);
		case MAX:
			if (bytesLength <= constraint) {
				break;
			}
			throw createIncorrectByteLengthException(dataTypeName, constraintType, constraint, bytesLength);
		}
	}

	private static IncorrectDataException createIncorrectByteLengthException(String dataTypeName,
			ConstraintType constraintType, int constraint, int bytesLength) {
		String message = String.format("Incorrect %s bytes size. Constraint type:%s, constraint:%d, found:%d",
				dataTypeName, constraintType.toString(), constraint, bytesLength);
		return new IncorrectDataException(message);
	}

	protected abstract T createData(byte[] bytes) throws IncorrectDataException;

	protected abstract String getTypeName();
}
