package tlvconverter.property_data_containers;

public class PropertyInfo {
	private String name;
	private Integer maxLength;

	public PropertyInfo(String name, Integer maxLength) {
		this.name = name;
		this.maxLength = maxLength;
	}

	public String getName() {
		return name;
	}

	public Integer getMaxLength() {
		return maxLength;
	}

}
