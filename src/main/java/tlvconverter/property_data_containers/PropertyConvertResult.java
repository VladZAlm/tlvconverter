package tlvconverter.property_data_containers;

public class PropertyConvertResult {
	private Object data;
	private byte[] bytesLeft;
	private PropertyType propertyType;

	public PropertyConvertResult(Object data, byte[] bytesLeft, PropertyType propertyType) {
		this.data = data;
		this.bytesLeft = bytesLeft;
		this.propertyType = propertyType;
	}

	public Object getData() {
		return data;
	}

	public byte[] getBytesLeft() {
		return bytesLeft;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

}
