package tlvconverter.property_data_containers;

import tlvconverter.data_type_creators.DataTypeCreator;
import tlvconverter.exceptions.IncorrectDataException;

public class PropertyType {
	protected String name;
	protected int tag;
	protected Integer length;
	protected DataTypeCreator<?> dataCreator;

	public PropertyType(String name, int tag, DataTypeCreator<?> dataCreator) {
		this(name, tag, null, dataCreator);
	}

	public PropertyType(String name, int tag, Integer length, DataTypeCreator<?> dataCreator) {
		this.name = name;
		this.tag = tag;
		this.length = length;
		this.dataCreator = dataCreator;
	}

	public String getName() {
		return name;
	}

	public int getTag() {
		return tag;
	}

	public Object getData(byte[] bytes) throws IncorrectDataException {
		PropertyInfo propertyInfo = new PropertyInfo(getPropertyName(), length);
		return dataCreator.create(bytes, propertyInfo);
	}

	public String getPropertyName() {
		return name + "(tag" + tag + ")";
	}

	public boolean getIsArray() {
		return dataCreator.getIsArray();
	}
}
