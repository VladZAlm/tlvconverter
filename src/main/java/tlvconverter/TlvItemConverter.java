package tlvconverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import tlvconverter.data_type_creators.BigIntegerCreator;
import tlvconverter.exceptions.IncorrectDataException;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyConvertResult;
import tlvconverter.property_data_containers.PropertyType;

public class TlvItemConverter {
	private int tagBytesLength;
	private int lengthBytesLength;
	private Map<Integer, PropertyType> propertyTypes;

	public TlvItemConverter(int tagBytesLength, int lengthBytesLength, Map<Integer, PropertyType> propertyTypes) {
		this.tagBytesLength = tagBytesLength;
		this.lengthBytesLength = lengthBytesLength;
		this.propertyTypes = propertyTypes;
	}

	public JSONObject convert(byte[] bytes) throws IncorrectItemDataException {
		return convert(bytes, false);
	}

	public JSONObject convert(byte[] bytes, boolean isSubItem) throws IncorrectItemDataException {
		Map<String, Object> properties = new HashMap<String, Object>();
		Set<Integer> foundPropertyTags = new HashSet<Integer>();
		Map<Integer, List<Object>> listProperties = initListProperties();

		int tagPosition = 0;
		if (isSubItem) {
			tagPosition = tagBytesLength + lengthBytesLength;
		}
		try {
			while (bytes.length != 0) {
				int previousBytesLength = bytes.length;
				byte[] bytesLeft = manageProperty(bytes, properties, foundPropertyTags, listProperties);
				bytes = bytesLeft;
				tagPosition += previousBytesLength - bytes.length;
			}

			tagPosition = 0;

			checkPropertiesCount(foundPropertyTags);

			fillPropertiesWithListProperties(properties, listProperties);

			return new JSONObject(properties);
		} catch (IncorrectDataException e) {
			if (e instanceof IncorrectItemDataException) {
				int eTagPosition = ((IncorrectItemDataException) e).getTagPosition();
				IncorrectItemDataException eCast = (IncorrectItemDataException) e;
				throw new IncorrectItemDataException(eCast, eTagPosition + tagPosition);
			}
			throw new IncorrectItemDataException(e, tagPosition);
		} catch (Exception e) {
			throw new IncorrectItemDataException(e, tagPosition);
		}
	}

	private byte[] manageProperty(byte[] bytes, Map<String, Object> properties, Set<Integer> foundPropertyTags,
			Map<Integer, List<Object>> listProperties) throws IncorrectDataException {
		PropertyConvertResult propertyConvertResult = getProperty(bytes);

		PropertyType propertyType = propertyConvertResult.getPropertyType();
		if (propertyType.getIsArray()) {
			List<Object> propertyList = listProperties.get(propertyType.getTag());
			propertyList.add(propertyConvertResult.getData());
		} else {
			if (foundPropertyTags.contains(propertyType.getTag())) {
				String message = "Non array property duplicate. Property name: " + propertyType.getPropertyName();
				throw new IncorrectDataException(message);
			}
			Object data = propertyConvertResult.getData();
			properties.put(propertyType.getName(), data);
		}

		foundPropertyTags.add(propertyType.getTag());
		return propertyConvertResult.getBytesLeft();
	}

	private void checkPropertiesCount(Set<Integer> foundPropertyTags) throws IncorrectDataException {
		int expectedPropertiesCount = propertyTypes.size();
		int foundPropertiesCount = foundPropertyTags.size();
		if (expectedPropertiesCount != foundPropertiesCount) {
			String message = String.format("Incorrect item properties count. Expected: %d, found: %d",
					expectedPropertiesCount, foundPropertiesCount);
			throw new IncorrectDataException(message);
		}
	}

	private Map<Integer, List<Object>> initListProperties() {
		Map<Integer, List<Object>> res = new HashMap<Integer, List<Object>>();
		for (Entry<Integer, PropertyType> entry : propertyTypes.entrySet()) {
			if (!entry.getValue().getIsArray()) {
				continue;
			}
			res.put(entry.getKey(), new ArrayList<Object>());
		}
		return res;
	}

	private void fillPropertiesWithListProperties(Map<String, Object> properties,
			Map<Integer, List<Object>> listProperties) {
		for (Entry<Integer, List<Object>> entry : listProperties.entrySet()) {
			String listPropertyName = propertyTypes.get(entry.getKey()).getName();
			JSONArray jsonArray = new JSONArray(entry.getValue());
			properties.put(listPropertyName, jsonArray);
		}
	}

	private PropertyConvertResult getProperty(byte[] bytes) throws IncorrectDataException {
		byte[] tagBytes = getSubBytes(bytes, 0, tagBytesLength);
		int tag = BigIntegerCreator.createUnsigned(tagBytes).intValue();
		PropertyType propertyType = propertyTypes.get(tag);
		if (propertyType == null) {
			Set<Integer> tags = propertyTypes.keySet();
			String message = String.format("Unknown tag: %d. List of expected: %s", tag, tags.toString());
			throw new IncorrectDataException(message);
		}
		byte[] lengthBytes = getSubBytes(bytes, tagBytesLength, lengthBytesLength);
		int length = BigIntegerCreator.createUnsigned(lengthBytes).intValue();
		byte[] valueBytes = getSubBytes(bytes, tagBytesLength + lengthBytesLength, length);
		Object data = propertyType.getData(valueBytes);
		byte[] bytesLeft = getSubBytes(bytes, tagBytesLength + lengthBytesLength + length);
		PropertyConvertResult res = new PropertyConvertResult(data, bytesLeft, propertyType);
		return res;
	}

	private byte[] getSubBytes(byte[] bytes, int start) throws IncorrectDataException {
		int length = bytes.length - start;
		return getSubBytes(bytes, start, length);
	}

	private byte[] getSubBytes(byte[] bytes, int start, int length) throws IncorrectDataException {
		if (bytes.length - start < length) {
			throw new IncorrectDataException("Not enough bytes");
		}
		int end = start + length;
		return Arrays.copyOfRange(bytes, start, end);
	}
}
