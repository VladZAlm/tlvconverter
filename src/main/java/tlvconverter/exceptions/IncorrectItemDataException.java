package tlvconverter.exceptions;

public class IncorrectItemDataException extends IncorrectDataException {
	private static final long serialVersionUID = -8519916282638251L;
	private int tagPosition;

	public IncorrectItemDataException(IncorrectDataException e, int tagPosition) {
		this(e.getMessage(), tagPosition);
	}

	public IncorrectItemDataException(IncorrectItemDataException e, int tagPosition) {
		this(e.getMsgWithoutTagPos(), tagPosition);
	}

	public IncorrectItemDataException(Throwable cause, int tagPosition) {
		super(cause);
		this.tagPosition = tagPosition;
	}

	private IncorrectItemDataException(String message, int tagPosition) {
		super(message);
		this.tagPosition = tagPosition;
	}

	public int getTagPosition() {
		return tagPosition;
	}

	@Override
	public String getMessage() {
		return super.getMessage() + ". Tag position in bytes array: " + tagPosition;
	}

	private String getMsgWithoutTagPos() {
		return super.getMessage();
	}

}
