package tlvconverter.exceptions;

public abstract class TlvConverterException extends Exception {
	private static final long serialVersionUID = -2678067671685001963L;

	public TlvConverterException(String message) {
		super(message);
	}

	public TlvConverterException(Throwable cause) {
		super(cause);
	}

}
