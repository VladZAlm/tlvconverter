package tlvconverter.exceptions;

public class IncorrectDataException extends TlvConverterException {
	private static final long serialVersionUID = -4502020421516351974L;

	public IncorrectDataException(String message) {
		super(message);
	}

	public IncorrectDataException(Throwable cause) {
		super(cause);
	}

}
