package tlvconverter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;

import org.json.JSONObject;

public class Runner {
	public static void main(String[] args) {
		try {
			if (args.length != 2) {
				System.out.println("Two parameters expected");
				return;
			}

			File file = new File(args[0]);
			if (!file.exists()) {
				System.out.println("File not found");
				return;
			}

			byte[] bytes = Files.readAllBytes(file.toPath());
			JSONObject json = OrderDocTlvConverter.convert(bytes);

			BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
			writer.write(json.toString());
			writer.close();

			System.out.println("done");
		} catch (Throwable t) {
			System.out.println(t.getMessage());
		}
	}
}
