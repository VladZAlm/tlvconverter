package tlvconverter;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import tlvconverter.data_type_creators.DataTypeCreator;
import tlvconverter.data_type_creators.DateCreator;
import tlvconverter.data_type_creators.FvlnCreator;
import tlvconverter.data_type_creators.ItemCreator;
import tlvconverter.data_type_creators.StringCreator;
import tlvconverter.data_type_creators.VlnCreator;
import tlvconverter.exceptions.IncorrectItemDataException;
import tlvconverter.property_data_containers.PropertyType;

public class OrderDocTlvConverter {

	private static final int TAG_LEN = 2;
	private static final int LENGTH_LEN = 2;

	public static JSONObject convert(byte[] bytes) throws IncorrectItemDataException {
		DataTypeCreator<?> dataCreator = new StringCreator();
		PropertyType name = new PropertyType("name", 11, 200, dataCreator);

		dataCreator = new VlnCreator();
		PropertyType price = new PropertyType("price", 12, 6, dataCreator);

		dataCreator = new FvlnCreator();
		PropertyType quantity = new PropertyType("quantity", 13, 8, dataCreator);

		dataCreator = new VlnCreator();
		PropertyType sum = new PropertyType("sum", 14, 6, dataCreator);

		Map<Integer, PropertyType> propertyTypes = new HashMap<Integer, PropertyType>();
		propertyTypes.put(name.getTag(), name);
		propertyTypes.put(price.getTag(), price);
		propertyTypes.put(quantity.getTag(), quantity);
		propertyTypes.put(sum.getTag(), sum);

		TlvItemConverter itemConverter = new TlvItemConverter(TAG_LEN, LENGTH_LEN, propertyTypes);

//------------------------------------------------------------------

		dataCreator = new DateCreator("yyyy-MM-dd'T'HH:mm:ss");
		PropertyType datetime = new PropertyType("dateTime", 1, dataCreator);

		dataCreator = new VlnCreator();
		PropertyType orderNumber = new PropertyType("orderNumber", 2, 8, dataCreator);

		dataCreator = new StringCreator();
		PropertyType customerName = new PropertyType("customerName", 3, 1000, dataCreator);

		dataCreator = new ItemCreator(itemConverter);
		dataCreator.setIsArray(true);
		PropertyType items = new PropertyType("items", 4, dataCreator);

		propertyTypes = new HashMap<Integer, PropertyType>();
		propertyTypes.put(datetime.getTag(), datetime);
		propertyTypes.put(orderNumber.getTag(), orderNumber);
		propertyTypes.put(customerName.getTag(), customerName);
		propertyTypes.put(items.getTag(), items);

		TlvItemConverter documentConverter = new TlvItemConverter(TAG_LEN, LENGTH_LEN, propertyTypes);
		return documentConverter.convert(bytes);
	}
}
